'''
@author: Mario Vitale - m3o

Game entry point.
Initialises the pygame library, the mixer, the screen, etc.
Creates a dict of the possible states with their corresponding classes
Creates a game instance passing the default start state (SPLASH)
Executes the functions: 
- run from the Game class which runs the game loop
- quit to close pygame once the loop ends
- exit to close the application after quitting pygame
'''

import sys
import pygame
import cfg
import init
from states.menu import Menu
from states.inter_level import InterLevel
from states.game_play import Gameplay
from states.game_over import GameOver
from states.serve import Serve
from states.splash import Splash
from states.pause import Pause
from game import Game

__author__ = "Mario Vitale - m3o"
__version__= "v0.1"
__title__ = cfg.TITLE


def main():
  
  welcome = "Welcome to "+__title__+" - "+__version__+" by "+__author__
  print(welcome)
  screen = pygame.display.set_mode(cfg.SCREENSIZE)

  pygame.display.set_caption(__title__+" - "+__version__)
  states = {
    "SPLASH": Splash(),
    "SERVE": Serve(),
    "GAMEPLAY": Gameplay(),
    "PAUSE": Pause(),
    "INTERLEVEL": InterLevel(),
    "GAMEOVER": GameOver(),
  }

  game = Game(screen, states, "SPLASH")
  game.run()

  pygame.mixer.quit()
  pygame.quit()
  sys.exit()

if __name__ == '__main__':
  main()