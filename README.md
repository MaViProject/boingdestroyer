# Boing Destroyer
Breakout clone in python + pygame library.  
Built using the FSM pattern.  


## Install the requirements  

```shell
$ pip install -r requirements.txt
```

<br/>

## Run the game  

```shell
$ python boingdest.py 
```
