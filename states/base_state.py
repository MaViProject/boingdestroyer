'''
@author: Mario Vitale - m3o

Base State,
it's an interface. 
implements the possible function a state must have (not all mandatory)
does not implement the logic
Sets the default font (can be overwritten in the state)
'''

import pygame
import cfg

class BaseState(object):
  def __init__(self):
    '''
    Set the state default properties
    '''
    self.done = False
    self.quit = False
    self.next_state = None
    self.screen_rect = pygame.display.get_surface().get_rect()
    self.persist = {}
    self.font = pygame.font.Font(cfg.FONT, cfg.FONT_BIG)

  def startup(self, persistent):
    '''
    Pass the persistent data to the new state
    '''
    self.persist = persistent

  def get_event(self, event):
    '''
    Listen for the events of the state
    '''
    # common to all the states
    if event.type == pygame.QUIT:
      self.quit = True


  def update(self, dt):
    '''
    To use if the state changes in the dt (delta)
    e.g.: the splash screen which last a number of seconds
    '''
    pass

  def draw(self, surface):
    '''
    What the state must draw on the screen
    '''
    pass