'''
@author: Mario Vitale - m3o

Initial Splash-screen, lasts 5 seconds then goes to the inter-level screen
'''

import os
import cfg
import init
import pygame
from .base_state import BaseState


class Splash(BaseState):
  def __init__(self):
    super(Splash, self).__init__()
    # Game title
    self.title = self.font.render(cfg.TITLE, True, pygame.Color("grey"))
    self.title_rect = self.title.get_rect(center=(cfg.SCREENSIZE[0]/2,self.title.get_height()/2))
    # splash screen background
    # get a random image and scale it to the screen size
    self.image = pygame.image.load(cfg.SPLASH_BKG_IMG)
    self.image = pygame.transform.scale(self.image, cfg.SCREENSIZE)
    self.img_rect = self.image.get_rect()

    self.next_state = "INTERLEVEL"
    self.time_active = 0
    # set the background music
    self.bkg_music = init.SPLASH_MUSIC
    self.bkg_music.set_volume(cfg.BKG_MUSIC_VOL)
    self.bkg_music.play(-1)


  def update(self, dt):
    self.time_active += dt
    if self.time_active >= cfg.SPLASH_TIME:
      self.bkg_music.stop()
      self.done = True

  def draw(self, surface):
    surface.fill(pygame.Color("black"))
    surface.blit(self.image, self.img_rect)
    surface.blit(self.title, self.title_rect)
  
