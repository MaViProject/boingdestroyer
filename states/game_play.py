'''
@author: Mario Vitale - m3o

State which manages the real game to play
'''

import os
import cfg
import init
import time
import pygame
import random
from states.base_state import BaseState
from actors.paddle import Paddle
from actors.boing import Boing
from actors.brick import Brick
from actors.heartswarm import HeartSwarm



class Gameplay(BaseState):
  def __init__(self):
    super(Gameplay, self).__init__()
    # set the font size for this state
    self.font = pygame.font.Font(cfg.FONT, cfg.FONT_SMALL)
    # default next-state is inter-level
    self.next_state = "INTERLEVEL"

  ### State-defined Functions ###

  def startup(self, persistent):
    '''
    overwrites the default as initializes the level
    persistent data are not available at init
    '''

    # gets the setup from previous state: serve
    self.persist = persistent
    # set a start random movement to the ball only if not already present (new play)
    if "boing" in self.persist:
      if self.persist["boing"].dx == 0:
        self.persist["boing"].dx = random.randint(-1, 1)
      if self.persist["boing"].dy == 0:
        self.persist["boing"].dy = - random.randint(1, 1)


  def get_event(self, event):
    if event.type == pygame.QUIT:
      self.quit = True
    self.persist["paddle"].get_event(event)
    if event.type == pygame.KEYDOWN:
      if event.key == pygame.K_SPACE:
        self.next_state = "PAUSE"
        self.done = True


  def update(self, dt):
    # update the game components using their own functions
    self.persist["paddle"].update(dt)
    self.persist["boing"].update(dt)

    # collision with paddle
    if self.persist["boing"].hit(self.persist["paddle"]):
      # ball is initialized outside the paddle and y is inverted
      self.persist["boing"].y = self.persist["paddle"].y - self.persist["paddle"].height/2
      self.persist["boing"].dy = - self.persist["boing"].dy

      # x get a direction and an angle according to the hit point (paddle divided in 3 parts)
      # first paddle part has been hit, then ball has an angle (paddle velocity/8 is added) - a little dx due to the position
      if self.persist["boing"].x < self.persist["paddle"].x + (self.persist["paddle"].width / 3):
        self.persist["boing"].dx = self.persist["boing"].dx + self.persist["paddle"].dx/8 - cfg.PADDLE_HIT_ANGLE_ADJ
      # third paddle part has been hit, then ball has an angle (paddle velocity/8 is added) + a little dx due to the position
      elif self.persist["boing"].x > self.persist["paddle"].x + (2*self.persist["paddle"].width / 3):
        self.persist["boing"].dx = self.persist["boing"].dx + self.persist["paddle"].dx/8 + cfg.PADDLE_HIT_ANGLE_ADJ
      # if is hit in the middle nothing change
      #elif (self.persist["boing"].x > self.persist["paddle"].x + (self.persist["paddle"].width / 3) and self.persist["boing"].x < self.persist["paddle"].x + (2*self.persist["paddle"].width / 3)): 
        #self.persist["boing"].x = self.persist["boing"].x

  
    # collision with brick
    for brk in self.persist["bricks"]:
      if self.persist["boing"].hit(brk):
        # increase the score
        self.persist["score"] = self.persist["score"] + cfg.BRICK_HIT_SCORE
        self.persist["scrtxt"].set_score(self.persist["score"])
        # set the ball's y behavior 
        # ball can hit bricks from top or bottom then must be differently initialized
        # hit from below
        if self.persist["boing"].y >= brk.y: 
          # set the ball's new position
          self.persist["boing"].y = brk.y + brk.height/2
        # hit from the top
        elif self.persist["boing"].y < brk.y: 
          self.persist["boing"].y = brk.y - brk.height/2
        # in all the cases invert the y
        self.persist["boing"].dy = - self.persist["boing"].dy
        
        # set the ball's x behavior
        # if hits the first third of the brick then dx goes on left
        if self.persist["boing"].x < brk.x + brk.width/3:
          self.persist["boing"].dx = - self.persist["boing"].dx - cfg.BRICK_HIT_ANGLE_ADJ
        elif self.persist["boing"].x > brk.x + (2*brk.width / 3):
          self.persist["boing"].dx = self.persist["boing"].dx + cfg.BRICK_HIT_ANGLE_ADJ
        # else hits in the middle and nothing change
        
        # set the brick (move to brick class)
        destroyed = brk.hit()
        if destroyed:
          self.persist["bricks"].remove(brk)

    # if balls flows below the screen is lost
    if self.persist["boing"].gone:
      self.persist["lives"] = self.persist["lives"] - 1
      #  update the heart swarm removing a heart
      self.persist["heartswarm"].set_lives(self.persist["lives"])
      # if 0 lives then game lost so next state is gameover
      if self.persist["lives"] == 0:
        self.next_state = "GAMEOVER"
        self.persist["message"] = "You lost!"
      # otherwise it serve another ball
      else:
        self.next_state = "SERVE"
      # move to the new state
      self.done = True

    # if all the bricks are destroyed then the level has been won
    if len(self.persist["bricks"]) == 0:
      self.persist["level"] = self.persist["level"]+1
      self.persist["message"] = "Level "+str(self.persist["level"])
      # renew wall and ball
      self.persist["bricks"] = {}
      self.persist["boing"].to_init = True
      self.next_state = "INTERLEVEL"
      # mov to the default next-state: interstate to recap
      self.done = True    
  

  def draw(self, surface):
    '''
    draw the board according to its status
    and highlight the cursor
    '''
    # clean up
    surface.fill(pygame.Color("black"))
    # draw the background
    surface.blit(self.persist["bkgimg"],self.persist["bkgimg_rect"])
    # other items
    self.persist["heartswarm"].draw(surface)
    self.persist["scrtxt"].draw(surface)
    self.persist["paddle"].draw(surface)
    self.persist["boing"].draw(surface)
    # wall
    for brk in self.persist["bricks"]:
      brk.draw(surface)
    
    
    


  
 