'''
@author: Mario Vitale - m3o

Inter-level splash-screen, lasts 2 seconds showing entering level nr and score, then goes to the next level serve
'''

import os
import cfg
import init
import pygame
from .base_state import BaseState


class InterLevel(BaseState):
  def __init__(self):
    super(InterLevel, self).__init__()
    # default title to view
    self.title = self.font.render("Level 1", True, pygame.Color("grey"))
    self.title_rect = self.title.get_rect(center=(cfg.SCREENSIZE[0]/2,self.title.get_height()/2))
    self.fontscore = pygame.font.Font(cfg.FONT, cfg.FONT_SMALL)
    # inter-level screen background
    # get a random image and scale it to the screen size
    self.image = pygame.image.load(cfg.SPLASH_BKG_IMG)
    self.image = pygame.transform.scale(self.image, cfg.SCREENSIZE)
    self.img_rect = self.image.get_rect()

    self.next_state = "SERVE"
    self.time_active = 0
    
    # set the background music
    self.bkg_music = init.INTER_LEV_MUSIC
    self.bkg_music.set_volume(cfg.BKG_MUSIC_VOL)
    self.bkg_music.play(-1)

  def startup(self, persistent):
    self.persist = persistent
    # reset the active time
    self.time_active = 0
    # set the message to show
    if self.persist:
      self.title = self.font.render(self.persist["message"], True, pygame.Color("grey"))
      # set the score to show and the rect (score only if there's a persist)
      self.score = self.fontscore.render("Your score: "+str(self.persist["score"]), True, pygame.Color("grey"))
      self.score_rect = self.score.get_rect(center=self.screen_rect.center)
    else:
      self.title = self.font.render("Level 1", True, pygame.Color("grey"))
      self.score = self.fontscore.render("Your score: 0", True, pygame.Color("grey"))
    # title and score rect is always to get
    self.title_rect = self.title.get_rect(center=(cfg.SCREENSIZE[0]/2,self.title.get_height()/2))
    self.score_rect = self.score.get_rect(center=self.screen_rect.center)


  def update(self, dt):
    self.time_active += dt
    if self.time_active >= cfg.INTER_LEV_TIME:
      self.bkg_music.stop()
      self.done = True

  def draw(self, surface):
    surface.fill(pygame.Color("black"))
    #surface.blit(self.image, self.img_rect)
    surface.blit(self.title, self.title_rect)
    surface.blit(self.score, self.score_rect)
