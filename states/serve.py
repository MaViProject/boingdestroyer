'''
@author: Mario Vitale - m3o

State before play where the ball is not moving, after space bar is pressed it starts
'''

import os
import cfg
import init
import time
import pygame
import random
from states.base_state import BaseState
from actors.paddle import Paddle
from actors.boing import Boing
from actors.brick import Brick
from actors.heartswarm import HeartSwarm
from actors.score import Score


class Serve(BaseState):
  def __init__(self):
    super(Serve, self).__init__()

    # instruction to move to play state
    self.font = pygame.font.Font(cfg.FONT, cfg.FONT_MEDIUM)
    self.fontscore = pygame.font.Font(cfg.FONT, cfg.FONT_SMALL)
    self.title = self.font.render("Press spacebar to play", True, pygame.Color("grey"))
    self.title_rect = self.title.get_rect(center=self.screen_rect.center)
    
    # image and all the sizing are done in previous state
    self.next_state = "GAMEPLAY"

  ### State-defined Functions ###

  def startup(self, persistent):
    '''
    overwrites the default as initializes the board
    persistent data are not available at init
    '''

    self.persist = persistent
   
    # play the music
    # if persistent data are coming from a previous state keep it otherwise init their values here as they must persist
    if not "bkgimg" in self.persist:
      self.persist["bkgimg"] = pygame.image.load(self.get_random_image(cfg.BKG_IMGS_PATH))
      self.persist["bkgimg"] = pygame.transform.scale(self.persist["bkgimg"], cfg.SCREENSIZE)
      self.persist["bkgimg_rect"] = self.persist["bkgimg"].get_rect()
    if not "lives" in self.persist:
      self.persist["lives"] = cfg.LIVES
    if not "score" in self.persist:
      self.persist["score"] = 0  
    if not "scrtxt" in self.persist:
      self.persist["scrtxt"] = Score(self.persist["score"])  
    if not "level" in self.persist:
      self.persist["level"] = 1
    if not "bricks" in self.persist or len(self.persist["bricks"]) == 0:
      self.persist["bricks"] = []
      # set a new level brick schema
      self.random_level_gen()
    if not "paddle" in self.persist:
      self.persist["paddle"] = Paddle(random.randint(0, 2))
    if not "boing" in self.persist or self.persist["boing"].to_init:
      print("init ball")
      self.persist["boing"] = Boing(random.randint(0, 1), self.persist["level"])
    if not "heartswarm" in self.persist:
      self.persist["heartswarm"] = HeartSwarm(self.persist["lives"])
    
    # init the paddle and ball positions
    self.persist["boing"].reset()
    self.persist["paddle"].reset()    
    

  def get_event(self, event):
    if event.type == pygame.QUIT:
      self.quit = True
    if event.type == pygame.KEYDOWN:
      if event.key == pygame.K_SPACE:
        self.done = True


  def draw(self, surface):
    '''
    draw the board according to its status
    and highlight the cursor
    '''
    # clean-up
    surface.fill(pygame.Color("black")) 
    # draw the background
    surface.blit(self.persist["bkgimg"],self.persist["bkgimg_rect"])
    # other items
    self.persist["paddle"].draw(surface)
    self.persist["boing"].draw(surface)
    self.persist["heartswarm"].draw(surface)
    self.persist["scrtxt"].draw(surface)
    # wall
    for brk in self.persist["bricks"]:
      brk.draw(surface)
    surface.blit(self.title, self.title_rect)
    

  
  def get_random_image(self, imgs_path):
    '''
    Gets the list of images in the path passed as parameter.
    Returns a random image full path from the list
    '''
    imagenames = os.listdir(imgs_path)
    assert len(imagenames) > 0
    return os.path.join(imgs_path, random.choice(imagenames))
  

  def random_level_gen(self):    
    '''
    Generates a random brick schema
    '''
    # randomly set the nr of rows and columns (even) within a range (for a better shape)
    rownum = random.randint(cfg.WALL_MIN_ROW_NR, cfg.WALL_MAX_ROW_NR)
    colnum = random.randint(cfg.WALL_MIN_COL_NR, cfg.WALL_MAX_COL_NR)
    # to be sure the number of columns it's even (for a better shape)
    colnum = colnum % 2 == 0 and (colnum + 1) or colnum

    print("The level has",rownum,"rows and",colnum,"columns")

    # cycle on the y: horizontal drawing (line by line)
    for y in range(0, rownum):
      # check if skip the line (more probable to not skip), if only 2 lines then don't skip
      if rownum > 2:
        skip_line = random.choice([True, False, False])
      else: 
        skip_line = False
      # decide if line alternates the brick shape
      alternate_shape = random.choice([True, False])
      # if alternates then this flag is used to switch, start with a random value
      altflag = random.choice([True, False])

      # brick id define the shape: 2 shapes per level. It's the integer to get the image in the BRICKS array
      # the more the level is hight, the more the images are various
      leveller = min(self.persist["level"], len(init.BRICKS))
      shape1 = random.randint(0, leveller)
      shape2 = random.randint(0, leveller)
      
      # basing on the rownum and colnum calculate the offset to center the bricks
      wall_width = (cfg.BRICK_WIDTH + cfg.BRICK_CLEARANCE)*colnum + cfg.BRICK_CLEARANCE*2 # clearance is considered also to the beginning and to the end
      x_offset = (cfg.GAME_WIDTH - wall_width)/2 + cfg.WALL_X_OFFSET_ADJ
      wall_height = (cfg.BRICK_HEIGHT + cfg.BRICK_CLEARANCE)*colnum + cfg.BRICK_CLEARANCE*2 # clearance is considered also to the beginning and to the end
      y_offset = (cfg.GAME_WIDTH - wall_width)/2 + cfg.WALL_Y_OFFSET_ADJ

      # for each column in the current row, define brick position and shape and enter in the array
      for x in range(0, colnum):
        # if not skip the line place a brick
        if not skip_line:
          # if not skip the single brick (most of the probability to not skip)
          skip_brick = random.randint(0, 3) == 1 and True or False
          if not skip_brick:
            # init an object having the brick characteristics and populate it
            br = {}
            br["x"] = x*(cfg.BRICK_WIDTH + cfg.BRICK_CLEARANCE) + x_offset
            br["y"] = y*(cfg.BRICK_HEIGHT + cfg.BRICK_CLEARANCE) + y_offset
            # deal with alternated shapes
            if (alternate_shape and altflag):
              br["id"] = shape2
              # switch so next brick has a different shape
              altflag = not altflag
            else:
              br["id"] = shape1
            # assign the number of "lives" (hit it can stands) to the brick, depends on the level
            br["lives"] = self.persist["level"] + random.randint(0, 1)
            # create the brick item passing the data
            brk = Brick(br)
            # add the brick to the array
            self.persist["bricks"].append(brk)
    # if the bricks are 4 or less 
    if len(self.persist["bricks"]) < 5:
      print("Due to the random, the wall has few bricks let's try again")
      self.random_level_gen()
    print("amount of bricks to draw",len(self.persist["bricks"]))


 
   