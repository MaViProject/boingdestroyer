'''
@author: Mario Vitale - m3o

State before play where the ball is not moving, after space bar is pressed it starts
'''

import os
import cfg
import init
import time
import pygame
import random
from states.base_state import BaseState
from actors.paddle import Paddle
from actors.boing import Boing
from actors.brick import Brick
#, boing, paddle


class Pause(BaseState):
  def __init__(self):
    super(Pause, self).__init__()
    # instruction to move to play state
    self.font = pygame.font.Font(cfg.FONT, cfg.FONT_MEDIUM)
    self.fontscore = pygame.font.Font(cfg.FONT, cfg.FONT_SMALL)
    self.title = self.font.render("Paused! Press spacebar to resume", True, pygame.Color("grey"))
    self.title_rect = self.title.get_rect(center=self.screen_rect.center)
    
    # image and all the sizing are done in previous state
    self.next_state = "GAMEPLAY"

  ### State-defined Functions ###

  def get_event(self, event):
    if event.type == pygame.QUIT:
      self.quit = True
    if event.type == pygame.KEYDOWN:
      if event.key == pygame.K_SPACE:
        self.done = True


  def draw(self, surface):
    '''
    draw the board according to its status
    and highlight the cursor
    '''
    # clean-up
    surface.fill(pygame.Color("black")) 
    # draw the background
    surface.blit(self.persist["bkgimg"],self.persist["bkgimg_rect"])

    self.persist["heartswarm"].draw(surface)
    self.persist["scrtxt"].draw(surface)
    self.persist["paddle"].draw(surface)
    self.persist["boing"].draw(surface)
    for brk in self.persist["bricks"]:
      brk.draw(surface)
    surface.blit(self.title, self.title_rect)
    