'''
@author: Mario Vitale - m3o

Game over state,
screen to manage the game over and what to do then (quit or start again)
'''

import cfg
import pygame
from .base_state import BaseState


class GameOver(BaseState):
  def __init__(self):
    super(GameOver, self).__init__()
    self.font = pygame.font.Font(cfg.FONT, cfg.FONT_SMALL)
    self.title = self.font.render("Game Over", True, pygame.Color("white"))
    self.title_rect = self.title.get_rect(center=self.screen_rect.center)
    self.instructions = self.font.render("Press Space to start again or Esc to quit", True, pygame.Color("white"))
    instructions_center = (self.screen_rect.center[0], self.screen_rect.center[1] +  50)
    self.instructions_rect = self.instructions.get_rect(center=instructions_center)


  def get_event(self, event):
    if event.type == pygame.QUIT:
      self.quit = True
    elif event.type == pygame.KEYUP:
      if event.key == pygame.K_SPACE:
        self.next_state = "SERVE"
        self.persist = {} # clean-up
        self.done = True
      elif event.key == pygame.K_ESCAPE:
        self.quit = True

  def draw(self, surface):
    surface.fill(pygame.Color("black"))
    surface.blit(self.title, self.title_rect)
    if self.persist["message"]:
      msg = self.font.render(self.persist["message"], False, pygame.Color("white"))
      msg_center = (self.screen_rect.center[0], self.screen_rect.center[1] - 40)
      msg_rect = msg.get_rect(center=msg_center)
      surface.blit(msg, msg_rect)
    surface.blit(self.instructions, self.instructions_rect)
    