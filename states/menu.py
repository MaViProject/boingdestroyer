'''
@author: Mario Vitale - m3o

Menu state,
Called after the splash-screen.
Allows the gamer to decide what to do (quit, start a game, etc).
'''

import cfg
import pygame
from .base_state import BaseState


class Menu(BaseState):
  def __init__(self):
    super(Menu, self).__init__()
    self.active_index = 0
    self.options = ["Small", "Medium", "Large", "Quit Game"]
    self.next_state = "SHOW"

  def render_text(self, index):
    # highlight the selected (active_index)
    color = pygame.Color("red") if index == self.active_index else pygame.Color("white")
    return self.font.render(self.options[index], True, color)

  def get_text_position(self, text, index):
    center = (self.screen_rect.center[0], self.screen_rect.center[1]-70 + (index * 50))
    return text.get_rect(center=center)

  def handle_action(self):
    '''
    perform the action according to the selected option list key
    '''
    # if option list is small, medium or large, store the value in persistent and done with this state
    if self.options[self.active_index] == "Small":
      self.persist["size"] = cfg.SIZE_S
      self.done = True
    elif self.options[self.active_index] == "Medium":
      self.persist["size"] = cfg.SIZE_M
      self.done = True
    elif self.options[self.active_index] == "Large":
      self.persist["size"] = cfg.SIZE_L
      self.done = True
    # otherwise quit
    elif self.active_index == 3:
      self.quit = True

  def range_options(self):
    '''modify the option index value if it's out of range'''
    if self.active_index <= 0 : self.active_index = 0
    elif self.active_index >= len(self.options)-1 : self.active_index = len(self.options)-1

  def get_event(self, event):
    if event.type == pygame.QUIT:
      self.quit = True
    # scroll through the options setting the key of the option list (avoiding scrolling more)
    elif event.type == pygame.KEYUP:
      if event.key == pygame.K_UP:
        self.active_index -= 1
        self.range_options()
      elif event.key == pygame.K_DOWN:
        self.active_index += 1 
        self.range_options()
      elif event.key == pygame.K_RETURN:
        self.handle_action()

  def draw(self, surface):
    surface.fill(pygame.Color("black"))
    for index, option in enumerate(self.options):
      text_render = self.render_text(index)
      surface.blit(text_render, self.get_text_position(text_render, index))