'''
@author: Mario Vitale - m3o

Score Actor: draws the score for each interested state. It is placed on top right to a second line
'''

import cfg
import pygame
from actors.actor import Actor



class Score(Actor):
  def __init__(self, scr):
    super(Score, self).__init__()
    self.score = scr
    # set the font
    self.fontscore = pygame.font.Font(cfg.FONT, cfg.FONT_SMALL)

  def set_score(self, score):
    self.score = score

      
  def draw(self, surface):
    scr_txt = self.fontscore.render("SCORE: "+str(self.score), True, pygame.Color("grey"))
    scr_txt_rect = scr_txt.get_rect()
    scr_size = self.fontscore.size("SCORE: "+str(self.score))
    score_place = (cfg.GAME_WIDTH-scr_size[0]+cfg.SCORE_CLEARANCE, 0+scr_size[1]*2) #y empirically consider the first line is occupy by heart swarm
    surface.blit(scr_txt, score_place, scr_txt_rect)

