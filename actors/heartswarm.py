'''
@author: Mario Vitale - m3o

Hearts Actor: manages the behavior of a swarm of hearts. Places it on top right in the screen.
'''

from actors.actor import Actor
import init
import cfg


class HeartSwarm(Actor):
  def __init__(self, lives):
    super(HeartSwarm, self).__init__()
    # default data, these must update and persist in level transition
    self.heart = init.ATLAS
    self.heart_swarm = []
    # init the swarm with the lives 
    self.lives = lives
    self.set_lives(self.lives)
    
  def set_lives(self, lives_left):
    '''
    set the hearts number and position according to the lives
    '''
    self.heart_swarm = []
    for l in range(self.lives):
      if lives_left != 0:
        heart_rect = init.HEARTS["full"]
        lives_left = lives_left - 1
      else: 
        heart_rect = init.HEARTS["empty"]
      heart_place = (cfg.GAME_WIDTH-(cfg.HEART_WIDTH+cfg.HEART_CLEARANCE)-(cfg.HEART_WIDTH+cfg.HEART_CLEARANCE)*l, 0+cfg.HEART_HEIGHT/2+cfg.HEART_CLEARANCE)
      hrt = {
        "rect": heart_rect,
        "place": heart_place 
      }
      self.heart_swarm.append(hrt)
      
  def draw(self, surface):
    for hrt in self.heart_swarm:
      surface.blit(self.heart, hrt["place"], hrt["rect"])
