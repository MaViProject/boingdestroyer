'''
@author: Mario Vitale - m3o

Manages the paddle and its behavior
'''


import cfg
import init
import pygame
from actors.actor import Actor

class Paddle(Actor):
  def __init__(self, shape):
    super(Paddle, self).__init__()
    # initialise paddle size
    self.width = cfg.PADDLE_WIDTH
    self.height = cfg.PADDLE_HEIGHT
    # init paddle size and shape
    self.size = 'medium' # the second size paddle (64x16)
    self.shape = shape
    # set the paddle position
    self.reset()
    # init paddle image and rect 
    self.paddle = init.ATLAS
    self.paddle_rect = init.PADDLES[self.shape][self.size]
    
    
  def get_event(self, event):
    if event.type == pygame.KEYDOWN:
      if event.key == pygame.K_LEFT:
        self.dx = -cfg.PADDLE_SPEED
      if event.key == pygame.K_RIGHT:
        self.dx = cfg.PADDLE_SPEED
    else:
      self.dx = 0
     

  def update(self, dt):
    # set the new place on the left stopping at the left border
    if self.dx < 0:
      self.x = max(0, self.x + self.dx * dt)
    # set the new place on the right stopping at the right border
    else:
      self.x = min(cfg.GAME_WIDTH - self.width, self.x + self.dx * dt)


  def draw(self, surface):
    surface.blit(self.paddle, (self.x, self.y), self.paddle_rect)


  def reset(self):
    '''
    when jumps in serve state set the paddle stationary in the middle
    constant values (4) are empiric adjustment
    '''
    # init paddle position
    self.x = cfg.GAME_WIDTH /2 - (self.width/2) 
    self.y = cfg.GAME_HEIGHT - (self.height) - cfg.PADDLE_HEIGHT_ADJ
    # init paddle velocity as stopped
    self.dy = 0
    self.dx = 0