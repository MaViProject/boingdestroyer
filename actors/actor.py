'''
@author: Mario Vitale - m3o

Base class to implement the items,
it's an interface. 
implements the possible function a state must have (not all mandatory)
does not implement the logic
Sets the default font (can be overwritten in the state)
'''

import pygame
import cfg

class Actor(object):
  def __init__(self):
    '''
    Set the state default properties.
    It's generic
    '''
    self.id = 0
    self.lives = 0
    self.score = 0
    self.x = 0
    self.y = 0
    self.width = 0
    self.height = 0
    self.gone = False
    self.next_state = None
    self.persist = {}

  def startup(self, persistent):
    '''
    Pass the persistent data to the new state
    '''
    self.persist = persistent


  def update(self, dt):
    '''
    To use if the state changes in the dt (delta time)
    e.g.: the splash screen which last a number of seconds
    '''
    pass

  def draw(self, surface):
    '''
    What the state must draw on the screen
    '''
    pass

  def hit(self, target):
    '''
    What to do when the object collides with something (the target)
    basing on rectangles overlaps
    '''
    pass