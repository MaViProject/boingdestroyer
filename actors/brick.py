'''
@author: Mario Vitale - m3o

Brick Actor: manages the behavior of each brick
'''

from actors.actor import Actor
import init
import cfg


class Brick(Actor):
  def __init__(self, brk):
    super(Brick, self).__init__()
    # default data, these must update and persist in level transition
    self.id = brk["id"]
    self.lives = brk["lives"]
    self.x = brk["x"]
    self.y = brk["y"]
    self.width = cfg.BRICK_WIDTH
    self.height = cfg.BRICK_HEIGHT
    # init brick image and rect 
    self.brick = init.ATLAS
    self.brick_rect = init.BRICKS[self.id]
    
  def hit(self):
    '''
    decrease the brick life and returns if it has been destroyed
    '''
    self.lives = self.lives - 1
    if self.lives == 0:
      brick_broken_sound = init.BRICK_BROKEN
      brick_broken_sound.play()
      return True
    else:
      return False
      
  def draw(self, surface):
    surface.blit(self.brick, (self.x, self.y), self.brick_rect)
