'''
@author: Mario Vitale - m3o

Manages the ball and its behavior
'''

import cfg
import init
import pygame
from actors.actor import Actor

class Boing(Actor):
  def __init__(self, color, level):
    super(Boing, self).__init__()
    # initialise the ball size 
    self.width = cfg.BOING_WIDTH
    self.height = cfg.BOING_HEIGHT
    # init ball color
    self.color = color
    # level-speed increment
    self.add_speed = (level-1) * cfg.BOING_SPEED_ADJ
    # init ball image from atlas and rectangle 
    self.boing = init.ATLAS
    self.boing_rect = init.BOINGS[self.color]
    # init the ball position and velocity via reset function
    self.to_init = False
    self.reset()

    #self.collision = False

  def hit(self, target):
    '''
    Returns if the ball collides or not with the target
    '''
    if self.x > target.x + target.width or target.x > self.x + self.width:
      return False
    if self.y > target.y + target.height or target.y > self.y + self.height:
      return False
    else:
      self.paddle_hit_sound = init.PADDLE_HIT_SND
      self.paddle_hit_sound.play()
      return True

  def reset(self):
    '''
    when jumps in serve state set the ball stationary in the middle
    constant values (4) are empiric adjustment
    '''
    # init position
    self.x = cfg.GAME_WIDTH / 2 - (self.width/2)
    self.y = cfg.GAME_HEIGHT - (self.height/2) - cfg.PADDLE_HEIGHT_ADJ - cfg.BOING_HEIGHT_ADJ
    # init the velocity as stopped
    self.dy = 0
    self.dx = 0
    # boing not lost
    self.gone = False



  def update(self, dt):
    self.x = self.x + self.dx * (dt + self.add_speed)
    self.y = self.y + self.dy * (dt + self.add_speed)
    # bounce against the left wall
    if self.x <= 0:
      self.x = 0
      self.dx = - self.dx 
      self.wall_hit_sound = init.WALL_HIT_SND
      self.wall_hit_sound.play()
    # bounce against the right wall
    if self.x >= cfg.GAME_WIDTH - self.width:
      self.x = cfg.GAME_WIDTH - self.width
      self.dx = - self.dx 
      self.wall_hit_sound = init.WALL_HIT_SND
      self.wall_hit_sound.play()
    # bounce against top wall
    if self.y <= 0:
      self.y = 0 + self.width
      self.dy = -self.dy 
      self.wall_hit_sound = init.WALL_HIT_SND
      self.wall_hit_sound.play()
    # doesn't bounce against bottom wall as you lose the ball (managed by play state)
    if self.y >= cfg.GAME_HEIGHT:
      self.ball_lost_sound = init.BALL_LOST_SND
      self.ball_lost_sound.play()
      self.gone = True

  
  def draw(self, surface):
    surface.blit(self.boing, (self.x, self.y), self.boing_rect)
    

