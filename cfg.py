'''
@author: Mario Vitale - m3o

Config file,
contains the game settings which are parametrized in the game.
'''

import os

TITLE = "Boing Destroyer"

# Game screen
GAME_WIDTH = 800
GAME_HEIGHT = 600
SCREENSIZE = (800, 600)
BACKGROUNDCOLOR = (255, 255, 255)


# Update frequency (frames per second)
FPS = 60
CLK = 25

# Paddle params
PADDLE_WIDTH = 64 #depending on the sprite
PADDLE_HEIGHT = 16 #depending on the sprite
PADDLE_SPEED = 1
PADDLE_HEIGHT_ADJ = 30
PADDLE_HIT_ANGLE_ADJ = 0.5

# Boing
BOING_WIDTH = 8 #depending on the sprite
BOING_HEIGHT = 8 #depending on the sprite
BOING_HEIGHT_ADJ = 20
BOING_SPEED_ADJ = 5 # incremental velocity per level

# BRICKS
BRICK_HEIGHT = 16 #depending on the sprite
BRICK_WIDTH = 32 #depending on the sprite
BRICK_CLEARANCE = 8 #space between bricks
BRICK_HIT_ANGLE_ADJ = 0.3
# random level grid config for the wall generation
WALL_MIN_COL_NR = 7
WALL_MAX_COL_NR = 13
WALL_MIN_ROW_NR = 2
WALL_MAX_ROW_NR = 5
# wall offset adjustment (if not needed centered)
WALL_Y_OFFSET_ADJ = -60
WALL_X_OFFSET_ADJ = 0
# score for each hit
BRICK_HIT_SCORE = 5

# Hearts
HEART_WIDTH = 10 #depending on the sprite
HEART_HEIGHT = 10 #depending on the sprite
HEART_CLEARANCE = 10 # space between the drawn hearts

# Paths 
resources_path = os.path.join(os.getcwd(),'resources')
imgs_path = os.path.join(resources_path,'imgs')
BKG_IMGS_PATH = os.path.join(imgs_path,'backgrounds')
print(BKG_IMGS_PATH)
audio_path = os.path.join(resources_path,'sounds')
font_path = os.path.join(resources_path,'fonts')

# Images
ATLAS = os.path.join(imgs_path,'breakout.png')
SPLASH_BKG_IMG = os.path.join(imgs_path,'splash.png')
# Font
FONT = os.path.join(font_path,'04b03.ttf')
FONT_BIG = 60
FONT_MEDIUM = 40
FONT_SMALL = 24
FONT_MIN = 14
SCORE_CLEARANCE = -10 # extra space close to the score

# Music
SPLASH_MUSIC = os.path.join(audio_path,'background.wav')
INTER_LEV_MUSIC = os.path.join(audio_path,'background.wav')
GAME_MUSIC = os.path.join(audio_path,'game.wav')
BRICK_HIT_SND = os.path.join(audio_path,'brick-hit.wav')
BRICK_BROKEN = os.path.join(audio_path,'boom.wav')
BALL_LOST_SND = os.path.join(audio_path,'ball_lost.wav')
PADDLE_HIT_SND = os.path.join(audio_path,'paddle_hit.wav')
WALL_HIT_SND =  os.path.join(audio_path,'wall_hit.wav')

BKG_MUSIC_VOL = 0.3

# Time Active ms
SPLASH_TIME = 1000
INTER_LEV_TIME = 1000



# Initial number of lives
LIVES = 4