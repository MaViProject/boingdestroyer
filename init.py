'''
@author: Mario Vitale - m3o

Manages the initialisations of the game:
- pylib libraries
- resources to avoid accessing several times to the files (creates list of dictionaries containing fonts, images, audio, etc.)
These are available as variable within the whole game.  
This is called while imported in the main 
Resources are dependent by the atlas and by the game: must be changed according to it and to the game. 
'''

import cfg
import pygame

### INIT PYGAME LIBS
pygame.init()
pygame.mixer.init()

### PARSING FUNCTIONS
def get_quads(tilew, tileh, start_x, start_y, end_x, end_y):
  '''
  Returns the quad (rectangle) of a certain size from a defined atlas portion.
  tilew and tileh are width and height of the tile: the sprite in the atlas.
  start_x and start_y define the point where to start the scan.
  end_x and end_y define the point where to end the scan.
  All the rects for the desired items are placed in the returned array.
  '''
  rects = []
  y = start_y
  while y < end_y:
    x = start_x
    while x < end_x:
      #print("Item starts at: (",x,",",y,")")
      rects.append(pygame.Rect(x, y, tilew, tileh))
      x = x + tilew
    y = y + tileh
  return rects

def get_bricks():
  '''
  Basing on the atlas gets the bricks
  Brick block is 32x16
  Scans an area and remove the last 3 blocks (balls, hearts and key-brick)
  '''
  bricks = get_quads(32,16,0,0,192,48)
  return bricks#[:-3]

def get_balls():
  '''
  Basing on the atlas gets the balls
  Ball block is 8x8
  Scans an area and remove the last block which is empty
  '''
  balls = get_quads(8,8,96,48,128,56) # gets only the first 3
  return balls#[:-1]

def get_hearts():
  '''
  Basing on the atlas gets the hearts directly
  Heart block is 10x10
  '''
  hearts = {
    'full': pygame.Rect(128, 48, 10, 10),
    'empty': pygame.Rect(138, 48, 10, 10)
  }
  return hearts

def get_bonus():
  '''
  Basing on the atlas gets the bonuses
  bonus block is 16x16
  The area to scan is precise so no further actions needed
  '''
  return get_quads(16,16,0,192,160,192)

def get_paddles():
  '''
  Basing on the atlas gets the paddles
  Paddle's dimension is not regular: for each color there are several sizes.
  Scans an area and fills the array with sub-arrays, one for each dimension
  '''
  paddles = []
  # paddles are located in the following area
  x = 0
  y = 64
  # 4 iterations, for each one gets all the sizes directly
  for i in range(0,3):
    paddles.append(
      {
        'small': pygame.Rect(x,y,32,16),
        'medium': pygame.Rect(x+32,y,64,16),
        'big': pygame.Rect(x+96,y,96,16),
        'huge': pygame.Rect(x,y+16,128,16)
      }
    )
    # set the position for the new paddle set of a different color
    x = 0
    y = y+32
  return paddles


### GLOBAL IMAGES VARIABLES 

# load the atlas file once
ATLAS = pygame.image.load(cfg.ATLAS)
# get the array of bricks quads (rectangles)
BRICKS = get_bricks()
# get the array of balls quads
BOINGS = get_balls()
# get the array of hearts quads
HEARTS = get_hearts()
# get the array of paddles quads
PADDLES = get_paddles()
# get the array of bonus quads
BONUS = get_bonus()


### GLOBAL SOUND VARIABLES

# load the splash screen background music
SPLASH_MUSIC = pygame.mixer.Sound(cfg.SPLASH_MUSIC) 
# load the splash screen background music
INTER_LEV_MUSIC = pygame.mixer.Sound(cfg.INTER_LEV_MUSIC) 
# load the game background music
GAME_MUSIC = pygame.mixer.Sound(cfg.GAME_MUSIC) 
# load the brick hit sound
BRICK_HIT_SND = pygame.mixer.Sound(cfg.BRICK_HIT_SND) 
# load the brick destroyed sound
BRICK_BROKEN = pygame.mixer.Sound(cfg.BRICK_BROKEN)  
# load the paddle hit sound
PADDLE_HIT_SND = pygame.mixer.Sound(cfg.PADDLE_HIT_SND)  
# load the ball lost sound
BALL_LOST_SND = pygame.mixer.Sound(cfg.BALL_LOST_SND)  
# load the wall hit sound
WALL_HIT_SND = pygame.mixer.Sound(cfg.WALL_HIT_SND)  

