'''
@author: Mario Vitale - m3o

Game class,
Calls the states and manages the state change.
It is instatiated by the main.py (entrypoint)
Implements the run method called by the main which is the game main-loop.
'''

import pygame
import cfg

class Game(object):
  def __init__(self, screen, states, start_state):
    '''
    Initialize the game with a clock, loads the states
    '''
    self.done = False
    self.screen = screen
    self.clock = pygame.time.Clock()
    self.fps = cfg.FPS
    self.clk = cfg.CLK
    self.states = states
    self.state_name = start_state
    self.state = self.states[self.state_name]

  def event_loop(self):
    '''
    Listen for the event and pass them to the 
    event management function of the current state
    '''
    for event in pygame.event.get():
      self.state.get_event(event)

  def flip_state(self):
    '''
    Move to one state to the other passing 
    values that must persist in the landing state
    '''
    current_state = self.state_name
    next_state = self.state.next_state
    self.state.done = False
    self.state_name = next_state
    persistent = self.state.persist
    self.state = self.states[self.state_name]
    self.state.startup(persistent)

  def update(self, dt):
    '''
    Listen for global events like quit or state change then
    calls the update function of the current state passing the delta
    '''
    if self.state.quit:
      self.done = True
    elif self.state.done:
      self.flip_state()
    self.state.update(dt)

  def draw(self):
    '''
    Calls the current state draw function on the passed screen
    '''
    self.state.draw(self.screen)

  def run(self):
    '''
    Runs the game's main loop performing all the actions and setting the delta-time
    until done is set to True.
    '''
    while not self.done:
      dt = self.clock.tick(self.fps)
      self.event_loop()
      self.update(dt)
      self.draw()
      self.clock.tick(self.clk)
      pygame.display.update()
